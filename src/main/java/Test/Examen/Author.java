package Test.Examen;

 class Author1 {
    private String name;
    private String email;
    private char gender;

    public Author1(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return this.name;
    }

    public String getEmail() {
        return this.email;
    }

    public char getGender() {
        return this.gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toString() {
        return this.name + " (" + this.gender + ") at " + this.email;
    }
}

  public class Author {
    public static void main(String[] args) {
        Author1 author = new Author1("Creanga", "nica.creanga@utcluj.ro", 'm');
        System.out.println(author.toString());
    }
}